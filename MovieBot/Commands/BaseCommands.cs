﻿using Discord.Commands;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using Discord;
using System.Linq;
using Discord.WebSocket;

namespace MovieBot.Commands
{
    public class BaseCommands : ModuleBase<SocketCommandContext>
    {
        readonly CommandService service;

        IConfiguration config;
        IMDBApi api;
        MovieRooster rooster;
        public BaseCommands(CommandService service, IConfiguration config, IMDBApi api, MovieRooster rooster)
        {
            this.service = service;
            this.config = config;
            this.api = api;
            this.rooster = rooster;
        }

        [Command("ping")]
        public async Task Ping()
        {
            await ReplyAsync($"pong {config.GetSection("Discord:CommandPrefix").Value}");
        }


        [Command("Search")]
        [Summary("Search for an a title | Usage: Search <string querry>")]
        public async Task Search([Remainder] String args)
        {
            var data = api.GetTop3(args);
            await ReplyAsync($"**Results:** `{data.Count}`");
            foreach (var x in data)
            {
                await ReplyAsync(embed: x.Build());
            }
        }

        [Command("Title")]
        [Summary("Get info about a title | Usage: Title <imdb id>")]
        public async Task Title(string id)
        {
            var embed = api.GetInfo(id);

            await ReplyAsync(embed: embed.Build());
        }

        [Command("List")]
        [Summary("Fill the movie channel")]
        public async Task List()
        {
            foreach(var id in rooster.AppData.IDs)
            {
                var embed = api.GetInfo(id);
                await AddEmbedToMovieChannel(embed);
            }
        }

        [Command("Add")]
        [Summary("Adds a title from movie channel | Usage: Add <imdb id>")]
        public async Task Add(string id)
        {
            if (rooster.AddID(id))
            {
                var embed = api.GetInfo(id);
                await AddEmbedToMovieChannel(embed);
            }
        }
        [Command("Remove")]
        [Summary("Removes a title from movie channel | Usage: Remove <imdb id>")]
        public async Task Remove(string id)
        {
            rooster.Remove(id);
            await RemoveMsgFromMovieChannel(id);
        }

        [Command("Top3")]
        [Summary("Lists Top3 titles")]
        public async Task Top3()
        {
            var moviesChannelID = ulong.Parse(config.GetSection("Discord:Channel").Value);
            var textChannel = Context.Guild.GetTextChannel(moviesChannelID);
            var messages = await textChannel.GetMessagesAsync().FlattenAsync();

            //var ordered = messages.Cast<IUserMessage>()
            //                      .OrderBy(x => x.Reactions.Values.First().ReactionCount);

            var ordered = messages.Cast<IUserMessage>()
                .OrderByDescending(x => x.Reactions.Values.First().ReactionCount);

            var top3 = ordered.Take(3);

            foreach (var x in top3)
            {
                var oldE = x.Embeds.First();
                var newE = new EmbedBuilder();
                newE.Description = oldE.Description;
                newE.ImageUrl = oldE.Image.Value.Url;
                newE.Footer = new EmbedFooterBuilder();
                newE.Footer.IconUrl = oldE.Footer.Value.IconUrl;
                newE.Footer.Text = oldE.Footer.Value.Text;
                //newE.Footer.Build();

                await ReplyAsync(embed: newE.Build());
            }
        }

        [Command("Clear")]
        [Summary("Clears the movie channel")]
        public async Task Clear()
        {
            await ReplyAsync("Cleaning the movie channel, 100 messages will be ");

            var moviesChannelID = ulong.Parse(config.GetSection("Discord:Channel").Value);
            var textChannel = Context.Guild.GetTextChannel(moviesChannelID);

            var messages = await textChannel.GetMessagesAsync().FlattenAsync();
            await textChannel.DeleteMessagesAsync(messages);
        }

        async Task AddEmbedToMovieChannel(EmbedBuilder embed)
        {
            var moviesChannelID = ulong.Parse(config.GetSection("Discord:Channel").Value);
            var textChannel = Context.Guild.GetTextChannel(moviesChannelID);
            var msg = await textChannel.SendMessageAsync(embed: embed.Build());

            

            await msg.AddReactionAsync(new Emoji("👍"));
            await msg.AddReactionAsync(new Emoji("👎"));
        }

        async Task RemoveMsgFromMovieChannel(string id)
        {
            IMessage toDelete = null;

            var moviesChannelID = ulong.Parse(config.GetSection("Discord:Channel").Value);
            var textChannel = Context.Guild.GetTextChannel(moviesChannelID);
            var messages = await textChannel.GetMessagesAsync().FlattenAsync();
            foreach(var x in messages)
            {
                foreach(var y in x.Embeds)
                {
                    if(y.Footer.Value.Text == id)
                    {
                        toDelete = x;
                        break;
                    }
                }
            }

            if(toDelete != null)
                await textChannel.DeleteMessageAsync(toDelete);
        }

        [Command("help")]
        [Summary("Get all available commands")]
        public async Task HelpAsync()
        {
            var prefix = this.config.GetSection("Discord:CommandPrefix");

            var builder = new EmbedBuilder()
            {
                Color = new Color(206, 206, 30),
                Description = "**Available commands:**"
            };

            foreach (var module in service.Modules)
            {
                string description = null;
                foreach (var cmd in module.Commands)
                {
                    var result = await cmd.CheckPreconditionsAsync(Context);
                    if (result.IsSuccess)
                        description += $"{prefix.Value}{cmd.Aliases.First()} - {cmd.Summary}\n";
                }

                if (!string.IsNullOrWhiteSpace(description))
                {
                    builder.AddField(x =>
                    {
                        x.Name = module.Name;
                        x.Value = description;
                        x.IsInline = false;
                    });
                }
            }

            await ReplyAsync("", false, builder.Build());
        }
    }
}
