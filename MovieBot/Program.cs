﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MovieBot
{
    class Program
    {
        static void Main(string[] args) => new Program()
                .RunBotAsync()
                .GetAwaiter()
                .GetResult();

        DiscordSocketClient client;
        CommandService commands;
        IServiceProvider services;
        IConfiguration config;
        public async Task RunBotAsync()
        {
            client = new DiscordSocketClient();
            commands = new CommandService();


            IConfiguration cfg = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            config = cfg;

            var rooster = new MovieRooster();

            var api = new IMDBApi(cfg);

            services = new ServiceCollection()
                .AddSingleton(client)
                .AddSingleton(commands)
                .AddSingleton(cfg)
                .AddSingleton(rooster)
                .AddSingleton(api)
                .BuildServiceProvider();

            var token = cfg.GetSection("Discord:Token");

            client.Log += ClientLog;
            await RegisterCommandsAsync();
            await client.LoginAsync(TokenType.Bot, token.Value.ToString());
            await client.StartAsync();
            await Task.Delay(-1);
        }

        Task ClientLog(LogMessage arg)
        {
            Console.WriteLine(arg);
            return Task.CompletedTask;
        }

        public async Task RegisterCommandsAsync()
        {
            client.MessageReceived += HandleCommandAsync;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly(), services);
        }
        async Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            var context = new SocketCommandContext(client, message);
            if (message.Author.IsBot) return;

            var prefix = config.GetSection("Discord:CommandPrefix");

            int argPos = 0;
            if(message.HasStringPrefix(prefix.Value, ref argPos))
            {
                var result = await commands.ExecuteAsync(context, argPos, services);
                if (!result.IsSuccess) Console.WriteLine(result.ErrorReason);
            }
        }
    }
}
