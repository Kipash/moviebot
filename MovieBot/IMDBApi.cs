﻿using Discord;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MovieBot
{
    public class IMDBApi
    {
        const string IMDBLogoUrl = @"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/New-imdb-logo.png/1200px-New-imdb-logo.png";
        const string NoPosterURL = @"https://www.globalenergy.com.sa/wp-content/uploads/2015/11/sempreview.jpg";

        IConfiguration config;
        public IMDBApi(IConfiguration config)
        {
            this.config = config;
        }

        public List<EmbedBuilder> GetTop3(string args)
        {
            List<EmbedBuilder> embeds = new List<EmbedBuilder>();

            var token = config.GetSection("imdb:Token").Value;

            var client = new RestClient($"https://movie-database-imdb-alternative.p.rapidapi.com/?page=1&r=json&s={args}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("x-rapidapi-host", "movie-database-imdb-alternative.p.rapidapi.com");
            request.AddHeader("x-rapidapi-key", token);
            IRestResponse response = client.Execute(request);

            var json = response.Content;

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(json);
            Console.ForegroundColor = ConsoleColor.White;

            var data = JsonSerializer.Deserialize<SearchData>(json);

            int max = data.Results > 3 ? 3 : data.Results;
            for (int i = 0; i < max; i++)
            {
                var title = data.Data[i];

                var embed = new EmbedBuilder();
                embed.ImageUrl = title.PosterURL != "N/A" ? title.PosterURL : NoPosterURL;
                
                //embed.Color = colors[i];
                embed.Description = $"**{title.Title}** ({title.Year})\n`{title.ID}`";

                embed.Footer = new EmbedFooterBuilder();
                embed.Footer.IconUrl = IMDBLogoUrl;
                embed.Footer.Text = $"{i + 1} of {max}";

                embeds.Add(embed);
            }

            return embeds;
        }
        public EmbedBuilder GetInfo(string id)
        {
            var token = config.GetSection("imdb:Token").Value;

            var client = new RestClient($"https://movie-database-imdb-alternative.p.rapidapi.com/?i={id}&r=json");
            var request = new RestRequest(Method.GET);
            request.AddHeader("x-rapidapi-host", "movie-database-imdb-alternative.p.rapidapi.com");
            request.AddHeader("x-rapidapi-key", token);
            IRestResponse response = client.Execute(request);

            var json = response.Content;

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(json);
            Console.ForegroundColor = ConsoleColor.White;


            var data = JsonSerializer.Deserialize<TitleData>(json);

            var embed = new EmbedBuilder();
            embed.ImageUrl = data.Poster != "N/A" ? data.Poster : NoPosterURL;
            embed.Description = $"**{data.Title}** (*__{data.Year}__) - {data.Runtime}*\n" +
                $"{string.Join(" ", data.Ratings.Select(x => $"`{x.Value}`"))}\n\n" +
                $"{data.Genre}\n" +
                $"{data.Director}";

            var colors = new Color[] { Color.DarkGrey, Color.DarkRed, Color.Blue, Color.Green };

            int i = (int)(float.Parse(data.imdbRating) / 2.5f);
            embed.Color = colors[i];

            embed.Footer = new EmbedFooterBuilder();
            embed.Footer.Text = data.imdbID;
            embed.Footer.IconUrl = IMDBLogoUrl;

            return embed;
        }
    }



    public class TitleData
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string Released { get; set; }
        public string Runtime { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public string Poster { get; set; }
        public string imdbRating { get; set; }
        public Rating[] Ratings { get; set; }
        public string imdbID { get; set; }
    }
    public class Rating
    {
        public string Source { get; set; }
        public string Value { get; set; }
    }

    public class SearchData
    {
        [JsonPropertyName("Search")]
        public SearchDataTitle[] Data { get; set; }

        [JsonPropertyName("totalResults")]
        public string ResultsRaw { set => Results = int.Parse(value); }

        public int Results;
    }
    public class SearchDataTitle
    {
        public string Title { get; set; }
        public string Year { get; set; }

        [JsonPropertyName("imdbID")]
        public string ID { get; set; }
        public string Type { get; set; }

        [JsonPropertyName("Poster")]
        public string PosterURL { get; set; }
    }
}