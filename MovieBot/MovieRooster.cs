﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace MovieBot
{
    public class MovieRooster
    {
        const string FileName = "AppData.json";
        public AppData AppData = new AppData();

        public MovieRooster()
        {
            Load();
        }

        public bool AddID(string id)
        {
            if (!AppData.IDs.Contains(id))
            {
                AppData.IDs.Add(id);
                UpdateAppData();

                return true;
            }

            return false;
        }

        public void Load()
        {
            if (File.Exists(FileName))
            {
                try
                {
                    var json = File.ReadAllText(FileName);

                    if (!string.IsNullOrWhiteSpace(json))
                        AppData = JsonSerializer.Deserialize<AppData>(json);
                }
                catch
                {
                    Console.WriteLine("Error reading AppData file");
                }
            }
            else
                UpdateAppData();
        }

        void UpdateAppData()
        {
            File.Delete(FileName);

            var json = JsonSerializer.Serialize<AppData>(AppData);
            File.WriteAllText(FileName, json);
        }

        public void Remove(string id)
        {
            AppData.IDs.Remove(id);
        }
    }

    public class AppData
    {
        public List<string> IDs { get; set; } = new List<string>();
    }
}
